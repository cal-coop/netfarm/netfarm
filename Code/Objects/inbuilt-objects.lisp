(in-package :netfarm)

;;; Inbuilt classes/schemas
(defclass inbuilt-netfarm-class (netfarm-class)
  ((schema-names :initarg :schema-name :reader inbuilt-netfarm-class-schema-names))
  (:documentation "A class for definining inbuilt Netfarm classes (which have names that are not hash names). 
Don't ever ever ever use this outside of the Netfarm system, or you will create inconsistent state w.r.t the rest of the world and no one will like you."))

(defvar *intern-hash-of-inbuilt-class?* t)

(defvar *inbuilt-classes* (make-hash-table :test 'equal)
  "A table that maps names to inbuilt classes.")

(defmethod netfarm-class-name ((class inbuilt-netfarm-class))
  (first (inbuilt-netfarm-class-schema-names class)))

(defmethod intern-class ((class inbuilt-netfarm-class))
  "Give us some control over when inbuilt classes are interned."
  (setf (gethash (netfarm-class-name class) *inbuilt-classes*) class)
  ;; We can't create the schema of the schema until the class has been bound.
  (let* ((schema (class->schema class))
         (inbuilt-name (schema-inbuilt-name schema)))
    (when *intern-hash-of-inbuilt-class?*
      (setf (%schema-inbuilt-name schema) '()
            (gethash inbuilt-name *classes*) class
            (%schema-inbuilt-name schema) inbuilt-name))
    (setf (gethash class *schemas*) schema)))

;;; Inbuilt objects (not classes)

(defvar *inbuilt-objects*
  (make-hash-table :test 'equal)
  "A table that maps names to inbuilt objects.")
(defvar *inbuilt-object-names*
  (make-hash-table :test 'eq)
  "A table that maps inbuilt objects to names.")

(defun make-named-instance (class name &rest initargs)
  (let ((instance (apply #'make-instance class initargs)))
    (setf (gethash name *inbuilt-objects*) instance)
    (setf (gethash instance *inbuilt-object-names*) name)
    instance))

(defun find-inbuilt-object (hash)
  "Returns the inbuilt object with given hash, or returns NIL if there isn't such an object."
  (values (gethash hash *inbuilt-objects* nil)))
