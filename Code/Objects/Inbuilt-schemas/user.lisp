(in-package :netfarm)

(netfarm-scripts::define-named-script *user-add-datum-script*
    "inbuilt@user-script" ("data" :true "I do not own the sender")
  (:method "add-datum" 1)
  ;; Make the expected author list (just myself).
  self (list 1)
  ;; Get the sender's author list.
  sender object-authors
  ;; If they aren't equal, throw an error.
  equal (jump-cond 0 4 0 0)
  (get-value 2) sender error
  ;; Otherwise, add their message as a computed value.
  (get-value 0) (get-env 1 0) add-computed-value
  (get-value 1) return)

(defclass user ()
  ((sign-key :initarg :sign-key :reader user-sign-key)
   (ecdh-key :initarg :ecdh-key :reader user-ecdh-key)
   (data     :computed t :reader user-data))
  (:metaclass inbuilt-netfarm-class)
  (:scripts *user-add-datum-script*)
  (:schema-name "inbuilt@user")
  (:documentation "An object that represents a Netfarm user, with signing and Elliptic Curve Diffie Hellman public keys. This object will also add received messages (sent from objects produced by the user, and the user only) to its associated data."))

(defun user-values (user name)
  (loop for computed-value in (user-data user)
        when (and (listp computed-value)
                  (equal (first computed-value) name))
          collect (second computed-value)))
