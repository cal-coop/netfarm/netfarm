(in-package :netfarm)

(alexandria:maphash-values (lambda (class)
                             (intern-class class))
                           *inbuilt-classes*)
