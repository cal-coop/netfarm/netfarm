(in-package :netfarm)

(netfarm-scripts:define-script *mutable*
    ("supersedes-object" "superseding?" "superseded-by" "add-successor")
  (:method "initialize" 0)
  ;; (unless (object-value self "superseding?") (return))
  self (get-value 1) object-value (jump-cond 0 1 0 0) return
  ;; (object-value self "supersedes-object")
  self (get-value 0) object-value
  ;; (call-method superseded "add-successor")
  (get-value 3) (call-method 0) return
  (:method "add-successor" 0)
  ;; (unless (equal (object-authors self) (object-authors sender)) (return))
  self object-authors sender object-authors equal
  (jump-cond 0 1 0 0) return
  ;; (add-computed-value "superseded-by" sender)
  (get-value 2) sender add-computed-value
  return)

(defclass mutable-mixin ()
  ((predecessor :initarg :supersedes
                :initform :false
                :accessor %mutable-predecessor)
   (succeeding? :initarg :superseding?
                :initform :false
                :accessor %mutable-predecessor-p)
   (successor :computed t :reader %mutable-successor))
  ;; This mixin is intentionally nameless, because it is pretty useless by
  ;; itself.
  (:metaclass netfarm-class)
  (:scripts *mutable*)
  (:documentation "A mixin that can provide an illusion of mutability."))

(defgeneric object-predecessor (object)
  (:documentation "Return the object (and T) that this object supersedes, or return NIL and NIL.")
  (:method (immutable-object) (values nil nil))
  (:method ((mutable mutable-mixin))
    (if (%mutable-predecessor-p mutable)
        (values (%mutable-predecessor mutable) t)
        (values nil nil))))

(defgeneric object-successors (object)
  (:method (immutable-object) '())
  (:method ((mutable mutable-mixin))
    (%mutable-successor mutable)))

(defgeneric update-object (object)
  (:method ((object mutable-mixin))
    (let ((new-object (allocate-instance (class-of object))))
      (overwrite-instance new-object object)
      (setf (%mutable-predecessor new-object) object
            (%mutable-predecessor-p new-object) :true)
      new-object)))
