(in-package :netfarm)

(netfarm-scripts::define-named-script *mirror-reflect-message-script*
    "inbuilt@mirror-script" ("This schema cannot be initialized.")
  (:method "initialize" 1)
  (get-value 0) (get-env 1 0) error
  (:method "does-not-understand" 2)
  ;; Recall that this frame looks like (next-methods method-name arguments)
  (get-env 2 0) car (get-env 1 0) (get-env 2 0) cdr apply-method
  ;; Get the value. If the method unwound, this will also unwind, because
  ;; it cannot take the CAR of #f.
  car return)

(defclass mirror-class ()
  ()
  (:metaclass inbuilt-netfarm-class)
  (:scripts *mirror-reflect-message-script*)
  (:schema-name "inbuilt@mirror-schema")
  (:documentation "The schema of an object that can 'reflect' messages in a way that makes the messages anonymous, by making the reference to the sender useless.

For example, this could be used in an authentication scheme where one object has to challenge another to present a reference to itself. Without a mirror, the other object could cheat by always presenting the sender object; but with a mirror, it cannot do so.
This schema cannot be initialized by the user."))

(defvar *mirror*
  (make-named-instance 'mirror-class "inbuilt@mirror"))
