(in-package :netfarm)

(defgeneric add-computed-value (object name value)
  (:method ((object object) name value)
    (push value (object-computed-values object name))
    t))
