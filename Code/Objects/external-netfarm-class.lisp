(in-package :netfarm)

(defclass external-netfarm-class (netfarm-class)
  ()
  (:documentation "The metaclass of a class we created by converting an external schema.
We perform some normalisation on Netfarm classes that are defined inside Lisp, but we should not normalise external classes."))

(defmethod name-vector ((class external-netfarm-class) names)
  "Don't sort the names of slots of external classes, as we don't control the layout of instances of those classes."
  (coerce names 'vector))

;;; Don't add backreferences to OBJECT and STANDARD-OBJECT for generated classes.
(defmethod closer-mop:add-direct-subclass ((class (eql (find-class 'object)))
                                           (subclass external-netfarm-class)))
(defmethod closer-mop:add-direct-subclass ((class
                                            (eql (find-class 'standard-object)))
                                           (subclass external-netfarm-class)))
