(in-package :netfarm-scripts)

(defstruct interpreter
  ;; Program state
  (program-counter 0 :type reasonably-sized-natural)
  (variables #() :type (simple-array t (*)))
  (program (make-array 0 :element-type '(unsigned-byte 8)
                         :initial-element 0)
           :type (simple-array (unsigned-byte 8) (*)))
  (call-stack '() :type list)
  (data-stack '() :type list)
  ;; Objects
  (current-object nil :type (or null object))
  (last-object    nil :type (or null object))
  (variables-cache (make-hash-table :test 'equal))
  ;; Environment
  (environment '() :type list)
  (entry-points (make-array 0 :element-type 'procedure-description)
                :type (simple-array procedure-description (*)))
  (side-effects '() :type list)
  (capabilities '() :read-only t)
  ;; Watchdog limits
  (instruction-count 0 :type fixnum))

(declaim (inline interpreter-read-byte interpreter-read-byte*))
(defun interpreter-read-byte (interpreter)
  (declare (interpreter interpreter))
  (prog1 (aref (interpreter-program interpreter)
               (interpreter-program-counter interpreter))
    (incf (interpreter-program-counter interpreter))))

(defun interpreter-read-byte* (interpreter &optional (offset 0))
  (declare (interpreter interpreter) (fixnum offset))
  (aref (interpreter-program interpreter)
        (+ offset (interpreter-program-counter interpreter))))

(declaim (inline step-interpreter))
(defun step-interpreter (interpreter operators)
  (declare (interpreter interpreter))
  (let* ((opcode (interpreter-read-byte interpreter))
         (function (aref operators opcode)))
    (incf (interpreter-instruction-count interpreter))
    (funcall function interpreter opcode)))

(defmacro pop* (place)
  `(if (null ,place)
       (error "stack underflow")
       (pop ,place)))

(defun entry-point-vector (entry-point-list)
  (let* ((vector (make-array (length entry-point-list)
                             :element-type 'procedure-description
                             :initial-element (make-procedure-description))))
    (loop for entry in entry-point-list
          for n from 0
          do (destructuring-bind (entry-point arguments) entry
               (setf (aref vector n)
                     (make-procedure-description
                      :entry-point entry-point
                      :arguments arguments))))
    vector))

(defun reset-counters (interpreter)
  (setf (interpreter-instruction-count interpreter) 0))

(defun setup-interpreter (script arguments &optional capabilities)
  (declare (script script)
           (list arguments))
  (let* ((program (%script-program-vector script))
         (variables (coerce (script-variables script) 'vector))
         (entry-points (entry-point-vector (script-entry-points script)))
         (interpreter
           (make-interpreter :capabilities capabilities))
         (description (aref entry-points 0)))
    (call-function interpreter
                   (make-procedure :entry-point (procedure-description-entry-point description)
                                   :entry-points entry-points
                                   :arguments (procedure-description-arguments description)
                                   :program program
                                   :variables variables
                                   :environment '())
                   arguments
                   :save-current nil)
    (reset-counters interpreter)
    interpreter))

(defun setup-method-interpreter (object method arguments &optional capabilities)
  (check-type object netfarm:object)
  (check-type method string)
  (let* ((interpreter (make-interpreter :capabilities capabilities))
         (*variables-cache* (interpreter-variables-cache interpreter))
         (*method-cache* (make-hash-table :test 'equal))
         (*entry-points-cache* (make-hash-table :test 'eq)))
    (call-object-method interpreter object method arguments)
    (when (equal (interpreter-data-stack interpreter) '("error"))
      (error 'method-missing
             :method method :object object))
    (setf (interpreter-call-stack interpreter) '())
    (reset-counters interpreter)
    interpreter))

(defun run-interpreter-by-stepping (interpreter cycle-limit print-cycles step)
  (declare ((or null fixnum) cycle-limit)
           (boolean print-cycles step))
  (let ((operators *operators*))
    (loop
      (block unwind-to-here
        (handler-bind ((done
                         (lambda (e)
                           (declare (ignore e))
                           (return-from run-interpreter-by-stepping)))
                       (error
                         (lambda (e)
                           (when print-cycles
                             (format t "~&Unwinding because of ~a~%" e))
                           (unless (typep e 'unrecoverable-error)
                             (unwind-interpreter interpreter
                                                 (lambda ()
                                                   (return-from unwind-to-here)))))))
          (loop
            (when print-cycles
              (let ((*print-circle* t))
                (print `(:pc ,(interpreter-program-counter interpreter)
                         :data ,(interpreter-data-stack interpreter)
                         :env ,(interpreter-environment interpreter))))
              (let ((opcode-info (aref *opcode-data* (interpreter-read-byte* interpreter))))
                (unless (null opcode-info)
                  (format t "(~a" (first opcode-info))
                  (loop for byte-name in (third opcode-info)
                        for offset from 1
                        for value = (interpreter-read-byte* interpreter offset)
                        do (format t " :~a ~d" byte-name value))
                  (format t ")~%"))))
            (step-interpreter interpreter operators)
            (when (and (not (null cycle-limit))
                       (> (interpreter-instruction-count interpreter) cycle-limit))
              (error "exceeded cycle limit"))
            (when step
              (read-line))))))))

(defun run-interpreter-by-dispatch-function (interpreter cycle-limit)
  (declare (special *dispatch-function*))
  (let ((dispatch-function *dispatch-function*))
    (tagbody
     continue
       (handler-case
           (funcall dispatch-function interpreter cycle-limit)
         (done ()
           (return-from run-interpreter-by-dispatch-function))
         (error (e)
           (unless (typep e 'unrecoverable-error)
             (unwind-interpreter interpreter
                                 (lambda ()
                                   (go continue))))
           (error e))))))

(defvar *script-machine-runner* #'run-interpreter-by-dispatch-function
  "A function that accepts an interpreter and cycle limit (or NIL), and modifies the interpreter in place with its final state. If it halted with an error, the error is signalled.")

(defun run-interpreter (interpreter &key cycle-limit print-cycles step)
  (declare (interpreter interpreter)
           ((or null fixnum) cycle-limit)
           (boolean print-cycles step))
  (ensure-dispatch-function)
  (let ((*method-cache* (make-hash-table :test 'equal))
        (*entry-points-cache* (make-hash-table :test 'eq))
        (*variables-cache* (interpreter-variables-cache interpreter)))
    (if (or print-cycles step)
        (run-interpreter-by-stepping interpreter cycle-limit
                                     print-cycles step)
        (funcall *script-machine-runner* interpreter cycle-limit)))
  (values (interpreter-data-stack interpreter) interpreter))
