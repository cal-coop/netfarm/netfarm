(in-package :netfarm-scripts)

;;; scymtym suggested SBCL's CASE might be faster now as it uses perfect
;;; hashing and a jump table to dispatch.

(defun make-dispatch-code ()
  `(tagbody
      (ecase opcode
        . ,(loop for position below 256
                 collect
                 `(,position
                   ,(if (null (aref *opcode-code* position))
                        `(go lose)
                        (destructuring-bind ((parts inputs bytes) declarations body)
                            (aref *opcode-code* position)
                          `(with-interpreter-parts-renamed (data-stack %%data-stack
                                                            program-counter %%program-counter
                                                            program %%program
                                                            . ,parts)
                             (let (,@(loop for input in (reverse inputs)
                                           collect `(,input (pop* %%data-stack)))
                                   ,@(loop for byte in bytes
                                           for position from 0
                                           collect `(,byte (aref %%program (+ ,position %%program-counter)))))
                               ,@declarations
                               (incf %%program-counter ,(length bytes))
                               ,body
                               (go out))))))))))
