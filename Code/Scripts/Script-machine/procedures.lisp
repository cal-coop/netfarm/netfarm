(in-package :netfarm-scripts)

(deftype reasonably-sized-natural ()
  "I hope this is a subtype of the implementation's FIXNUM. But it may not be." 
  '(unsigned-byte 30))
(deftype interpreter-boolean ()
  '(member :true :false))
(deftype interpreter-datum ()
  `(or integer
       string
       (simple-array (unsigned-byte 8) (*))
       interpreter-boolean
       procedure
       list
       object))

(declaim (inline make-procedure))
(defstruct procedure
  (entry-point 0 :type reasonably-sized-natural)
  (arguments 0 :type reasonably-sized-natural)
  (program (error "no PROGRAM") :type (simple-array (unsigned-byte 8) (*)))
  (variables (error "no VARIABLES") :type simple-vector)
  (object nil :type (or object null))
  (environment '() :type list)
  (entry-points))

(defstruct procedure-description
  (entry-point 0 :type reasonably-sized-natural)
  (arguments 0 :type reasonably-sized-natural))

(defvar *method-cache* (make-hash-table :test 'equal))
(defvar *entry-points-cache* (make-hash-table :test 'eq))
(defvar *variables-cache* (make-hash-table :test 'equal))

(defun compute-method-procedure-descriptions (class method-name)
  (netfarm::with-cache ((cons method-name class) *method-cache*)
    (loop for script in (netfarm:netfarm-class-scripts class)
          for method = (gethash method-name (script-method-table script))
          unless (null method)
            collect (destructuring-bind (entry-point arguments) method
                      (list entry-point arguments script)))))
          
(defun compute-method-procedures (object method-name)
  (unless (typep object 'netfarm:object)
    (return-from compute-method-procedures '()))
  (netfarm::with-cache ((cons method-name object) *method-cache*)
    (loop for (entry-point arguments script)
            in (compute-method-procedure-descriptions (class-of object)
                                                      method-name)
          collect (make-procedure
                   :entry-points (netfarm::with-cache (script *entry-points-cache*)
                                   (entry-point-vector
                                    (script-entry-points script)))
                   :entry-point entry-point
                   :arguments (1+ arguments)
                   :program (%script-program-vector script)
                   :variables (netfarm::with-cache ((cons object script) *variables-cache*)
                                (coerce (script-variables script) 'vector))
                   :object object
                   :environment '()))))

(defun call-method-strategy (object method-name)
  "Decide what CALL-METHOD should do.
We either return the values :methods and a list of methods, should we have appropriate methods; :does-not-understand and a list of methods if there are methods for does-not-understand; or :no-methods and the empty list should there be no applicable methods."
  (unless (typep object 'netfarm:object)
    ;; There couldn't possibly be methods on a value that isn't an object.
    (return-from call-method-strategy (values :no-methods '())))
  (let ((methods (compute-method-procedures object method-name)))
    (unless (null methods)
      (return-from call-method-strategy (values :methods methods)))
    (let ((does-not-understand-methods
            (compute-method-procedures object "does-not-understand")))
      (unless (null does-not-understand-methods)
        (return-from call-method-strategy
          (values :does-not-understand does-not-understand-methods)))
      (values :no-methods '()))))
