(in-package :netfarm-scripts)

;; 22: drop x --
(define-opcode* 34 drop (() x) ()
  (values))

;; 23: dup x -- x x
(define-opcode* 35 dup ((:data-stack data-stack) x) ()
  (push x data-stack)
  (push x data-stack))

;; 24: swap x y -- y x
(define-opcode* 36 swap ((:data-stack data-stack) x y) ()
  (push y data-stack)
  (push x data-stack))
