(in-package :netfarm-scripts)

(defmacro define-script (variable-name (&rest variables) &body body)
  (multiple-value-bind (entry-points methods program)
      (assemble body)
    `(defparameter ,variable-name
       (make-instance 'script
                      :entry-points ',entry-points
                      :program ',program
                      :methods ',methods
                      :variables ',variables))))

(defmacro define-named-script (variable-name inbuilt-name (&rest variables) &body body)
  (multiple-value-bind (entry-points methods program)
      (assemble body)
    `(defparameter ,variable-name
       (netfarm::make-named-instance 'script
                                     ,inbuilt-name
                                     :entry-points ',entry-points
                                     :program ',program
                                     :methods ',methods
                                     :variables ',variables))))
