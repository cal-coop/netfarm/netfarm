(in-package :netfarm-scripts)

(defvar *script-instruction-limit* 300000)

(defun apply-recipient-filter (effects filter)
  "Keep all effects allowed by the filter."
  (remove-if-not filter effects))

(defun run-script-machines (object &key (apply-side-effects t)
                                        (recipient-filter (constantly t)))
  (check-type object object)
  (handler-case (setup-method-interpreter object "initialize"
                                          '()
                                          '(:write-computed-values))
    (:no-error (interpreter)
      (multiple-value-prog1
          (run-interpreter interpreter :cycle-limit *script-instruction-limit*)
        (when apply-side-effects
          (apply-side-effects
           (apply-recipient-filter (interpreter-side-effects interpreter)
                                   recipient-filter)))))
    (error ()
      (warn "No initialize method on ~s" object))))

(defvar *capabilities* '())
(defun run-script (script &rest arguments)
  (values-list
   (run-interpreter
    (setup-interpreter script arguments *capabilities*))))
(defun send-message (object method-name &rest arguments)
  (values-list
   (run-interpreter
    (setup-method-interpreter object method-name arguments *capabilities*))))
