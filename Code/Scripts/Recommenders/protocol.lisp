(in-package :netfarm-scripts)

(defclass recommender () ())

(defgeneric sort-object-list (recommender objects query end))

(defmacro with-recommender ((recommender) &body body)
  (alexandria:once-only (recommender)
    `(with-object-sorter ((lambda (list number-wanted query reverse?)
                            (let ((sorted (sort-object-list ,recommender
                                                            list
                                                            query
                                                            number-wanted)))
                              (if reverse?
                                  (coerce (reverse sorted) 'list)
                                  (coerce sorted 'list)))))
       ,@body)))

(defun substitute-object-into-query (object query)
  (substitute object +current-object-variable+ query))
