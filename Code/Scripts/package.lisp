(defpackage :netfarm-scripts
  (:use :cl :netfarm)
  (:export
   #:*script-instruction-limit* #:*capabilities* #:*script-machine-runner*
   #:netfarm-equal
   ;; assembler
   #:assemble #:define-script
   #:script #:opcode-information
   ;; interpreter accessors
   #:interpreter #:interpreter-side-effects
   #:interpreter-instruction-count #:interpreter-cons-count
   #:run-interpreter #:setup-interpreter #:setup-method-interpreter
   #:method-missing
   ;; effects
   #:add-computed-value #:remove-computed-value #:set-counters
   #:run-script-machines #:run-script #:send-message #:script-machine-error
   #:with-consistent-state #:apply-side-effects-on #:apply-side-effect
   ;; recommender and presentations
   #:*default-presentation-script*
   #:recommender #:sort-object-list #:substitute-object-into-query
   #:with-object-sorter #:with-recommender #:compute-presentation))
