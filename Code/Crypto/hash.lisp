(in-package :netfarm)

(declaim (inline hash-text hash-bytes))
(defun hash-text (text)
  (declare (string text))
  (digest-sequence :sha256 (babel:string-to-octets text :encoding :utf-8)))
(defun hash-bytes (bytes)
  (digest-sequence :sha256 (coerce (subseq bytes 0 (length bytes))
                                   '(vector (unsigned-byte 8)))))

(defconstant +buffer-length+ 32768)
(defun base-hash-object (object &key (emit-signatures t))
  (declare (optimize (speed 3)))
  (let ((digest (make-digest :sha256))
        (*rewrite-references?* nil)
        (buffer (make-array +buffer-length+
                            :element-type '(unsigned-byte 8)))
        (buffer-position 0))
    (declare ((mod 32768) buffer-position))
    (labels ((flush (all?)
               (if all?
                   (update-digest digest buffer)
                   (update-digest digest buffer :end buffer-position))
               (setf buffer-position 0))
             (feed (vector)
               (declare ((simple-array (unsigned-byte 8) 1) vector))
               (when (> (length vector) +buffer-length+)
                 ;; If the vector is longer than the buffer, flush the buffer
                 ;; then update the digest with the vector.
                 (flush nil)
                 (update-digest digest vector)
                 (return-from feed))
               ;; Else, fill in the buffer.
               (replace buffer vector :start1 buffer-position)
               (cond
                 ((>= (+ buffer-position (length vector))
                      +buffer-length+)
                  (let ((start-of-next-part (- +buffer-length+ buffer-position)))
                    (flush t)
                    ;; Copy in the part of the vector we didn't get into the
                    ;; buffer before.
                    (replace buffer vector
                             :start2 start-of-next-part)
                    (setf buffer-position (- (length vector) start-of-next-part))))
                 (t
                  (incf buffer-position (length vector))))))
      (binary-render-object object :emit-signatures emit-signatures
                                   :emit-computed-values nil
                                   :function #'feed)
      (flush nil))
    (produce-digest digest)))

(defvar *hash-cache-lock*)
(defvar *hash-cache* nil
  "Either NIL or a table mapping objects to text hashes.")

(defmacro %with-hash-cache (() &body body)
  `(let ((*hash-cache-lock* (bt:make-lock "Hash cache lock"))
         (*hash-cache* (trivial-garbage:make-weak-hash-table :weakness :key
                                                             :test 'eq)))
     ,@body))

(defmacro with-hash-cache ((&key keep-old) &body body)
  "Allow Netfarm to cache hashes in the body.
If KEEP-OLD is true, and there already is a hash cache, use that one instead."
  (if keep-old
      (alexandria:with-gensyms (cache-present? continuation)
        `(let ((,cache-present? (boundp '*hash-cache*)))
           (flet ((,continuation ()
                    ,@body))
             (if ,cache-present?
                 (,continuation)
                 (%with-hash-cache ()
                   (,continuation))))))
      `(%with-hash-cache () ,@body)))
(defmacro with-lexical-hash-cache (() &body body)
  "Store hash caches in a lexical variable, and locally define a macro WITH-RESTORED-LEXICAL-HASH-CACHE to restore the cache."
  (alexandria:with-gensyms (hash-cache hash-cache-lock)
    `(let ((,hash-cache nil) (,hash-cache-lock nil))
       (unless (null *hash-cache*)
         (setf ,hash-cache-lock *hash-cache-lock*
               ,hash-cache *hash-cache*))
       (macrolet ((with-restored-lexical-hash-cache (() &body body)
                    `(let ((*hash-cache* ,',hash-cache)
                           (*hash-cache-lock* ,',hash-cache-lock))
                       ,@body)))
         ,@body))))
                

(defgeneric hash-object (object)
  (:documentation "Generate a binary-hash for an object, including its signatures but not computed values.")
  (:method ((schema schema))
    (if (null (schema-inbuilt-name schema))
        (call-next-method)
        (error "Cannot binary-hash an inbuilt schema")))
  (:method :around ((object object))
    (when (gethash object *inbuilt-object-names*)
      (error "Cannot binary-hash an inbuilt object"))
    (unless (null *hash-cache*)
      (multiple-value-bind (hash-text present?)
          (bt:with-lock-held (*hash-cache-lock*)
            (gethash object *hash-cache*))
        (when present?
          (handler-case (base64->bytes hash-text)
            (:no-error (hash)
              (return-from hash-object hash))
            (error ())))))
    (let ((hash (call-next-method)))
      (unless (null *hash-cache*)
        (bt:with-lock-held (*hash-cache-lock*)
          (setf (gethash object *hash-cache*)
                (bytes->base64 hash))))
      hash))
  (:method ((object object))
    (base-hash-object object))
  (:method ((vague-object vague-object))
    (base-hash-object vague-object))
  (:method ((reference reference))
    (base64->bytes (reference-hash reference))))

(defgeneric hash-object* (object)
  (:documentation "Generate a text-hash for an object.")
  (:method :around (object)
    (cond
      ((gethash object *inbuilt-object-names*))
      ((null *hash-cache*) (call-next-method))
      ((bt:with-lock-held (*hash-cache-lock*) (gethash object *hash-cache*)))
      (t (let ((hash (call-next-method)))
           (bt:with-lock-held (*hash-cache-lock*)
             (setf (gethash object *hash-cache*) hash))))))
  (:method ((schema schema))
    (if (null (schema-inbuilt-name schema))
        (call-next-method)
        (schema-inbuilt-name schema)))
  (:method ((object object))
    (bytes->base64 (hash-object object)))
  (:method ((vague-object vague-object))
    (bytes->base64 (hash-object vague-object)))
  (:method ((reference reference))
    (reference-hash reference)))

(defun verify-object-hash (object hash)
  "Verify a total hash as generated by HASH-OBJECT."
  (let ((their-hash (alexandria:copy-array (subseq hash 0 32)
                                           :element-type '(unsigned-byte 8)
                                           :fill-pointer nil
                                           :adjustable nil))
        (our-hash (hash-object object)))
    (ironclad:constant-time-equal their-hash our-hash)))

;;; Pronounceable verification codes.
;; Not dissimilar to https://github.com/deoxxa/proquint/blob/master/encode.js
;; Not awfully similar, either.

(defvar *vowels* "AEOU")
(defvar *consonants* "BFGLMRST")

(defun readable-verifier (digest)
  "Map each byte in the byte array DIGEST (which probably should be a digest, to be fair) to words in a somewhat readable \"sentence\". This function should only be used for \"presenting\" hashes and those kind of values to users for verification, and not stored.

Each byte is written in a way where the bit pattern is mapped to CCCVVCCC or something like that. We tried to pick sounds that are hard to confuse; but you probably should just M-. this function and read the surroundings if you're curious about how this is implemented."
  (format nil "~{~a ~(~a ~a ~a~)~^, ~a ~(~a ~a ~a~)~^,~%~}"
          (loop for byte across digest
                collect (format nil "~c~(~c~c~)"
                                (char *consonants*
                                      (ldb (byte 3 5) byte))
                                (char *vowels*
                                      (ldb (byte 2 3) byte))
                                (char *consonants*
                                      (ldb (byte 3 0) byte))))))
