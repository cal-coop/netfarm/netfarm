(in-package :netfarm)

(define-condition parser-error (simple-error)
  ())
(defun parser-error (control &rest arguments)
  (error 'parser-error :format-control control :format-arguments arguments))
(defun parser-assert (test control &rest arguments)
  (unless test
    (apply #'parser-error control arguments)))

(defvar *maximum-byte-vector-length* 1000000)
(defvar *maximum-storage-vector-length* 1000)
(defvar *maximum-list-length* 1000)

(defun read-integer (function)
  (let ((byte-count (funcall function))
        (accumulator 0))
    (dotimes (n byte-count)
      (setf accumulator
            (logior (funcall function)
                    (ash accumulator 8))))
    accumulator))

(defun read-byte-vector (function count)
  (parser-assert (<= count *maximum-byte-vector-length*)
                 "Not going to create a byte array with ~d bytes"
                 count)
  (let ((vector (make-array count :element-type '(unsigned-byte 8))))
    (dotimes (n count)
      (setf (aref vector n) (funcall function)))
    vector))

(defun read-string (function)
  (babel:octets-to-string (read-byte-vector function
                                            (read-integer function))
                          :encoding :utf-8))

(defvar *in-slot?* nil)

(defun read-vector (function)
  (let ((length (read-integer function)))
    (parser-assert (<= length *maximum-storage-vector-length*)
                   "Not going to create a storage vector with ~d elements"
                   length)
    (let ((vector (make-array length)))
      (dotimes (n length)
        (setf (aref vector n)
              (binary-parse-from-function function)))
      vector)))

(defun binary-parse-from-type-tag (type-tag function)
  (case type-tag
    (1
     (read-string function))
    (2
     (let ((length (read-integer function)))
       (read-byte-vector function length)))
    (3
     (read-integer function))
    (4
     (let ((integer (read-integer function)))
       (if (zerop integer)
           (parser-error "negative-integer tag used for 0")
           (- integer))))
    (5
     (parser-assert (plusp *reader-depth*) "Too much list recursion")
     (let ((length (read-integer function))
           (*in-slot?* nil))
       (parser-assert (<= length *maximum-list-length*)
                      "Not going to create a list with ~d elements"
                      length)
       (loop repeat length
             collect (binary-parse-from-function function))))
    (6
     (make-instance 'reference
                    :hash (read-string function)))
    (7 :true)
    (8 :false)
    (9
     (parser-assert *in-slot?*
                    ":unbound can't appear outside a slot value")
     :unbound)
    (otherwise
     (parser-assert nil "Unknown type tag ~d" type-tag))))

(defun binary-parse-from-function (function)
  (binary-parse-from-type-tag (funcall function) function))

(defun binary-parse (function-or-vector)
  (if (vectorp function-or-vector)
      (let ((position 0)
            (vector function-or-vector))
        (binary-parse-from-function
         (lambda ()
           (prog1 (aref vector position)
             (incf position)))))
      (binary-parse-from-function function-or-vector)))

(defun read-signatures (function)
  (loop repeat (read-integer function)
        collect (cons (make-instance 'reference
                                     :hash (read-byte-vector function 32))
                      (read-byte-vector function 64))))

(defun read-hash-table (function)
  (let ((size (read-integer function)))
    (parser-assert (= size 1)
                   "We only have one metadata element currently")
    (let ((table (make-hash-table :test 'equal :size size)))
      (dotimes (n size)
        (setf (gethash (binary-parse-from-function function) table)
              (binary-parse-from-function function)))
      table)))

(defun binary-parse-block-from-function (function &key source name)
  (make-instance 'vague-object
                 :name name :source source
                 :signatures (read-signatures function)
                 :metadata (read-hash-table function)
                 :values   (let ((*in-slot?* t))
                             (read-vector function))
                 :computed-values (read-vector function)))

(defun binary-parse-block (function-or-vector &key source name)
  (if (vectorp function-or-vector)
      (let ((position 0)
            (vector function-or-vector))
        (binary-parse-block-from-function
         (lambda ()
           (prog1 (aref vector position)
             (incf position)))
         :source source :name name))
      (binary-parse-block-from-function function-or-vector
                                        :source source
                                        :name name)))
