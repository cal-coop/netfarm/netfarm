(in-package :netfarm)

(defvar *in-slot?* nil)
(defun render-hash-table (hash-table stream)
  (loop for ((key . value) . rest?)
          on (sort (alexandria:hash-table-alist hash-table) #'string< :key #'car)
        do (render key stream)
           (write-char #\Space stream)
           (render value stream)
           (when rest?
             (write-char #\Newline stream))))

(defun render-slots (object stream)
  (let* ((class (class-of object))
         (slots (netfarm-class-slot-names class)))
    (loop with length = (length slots)
          for slot-name across slots
          for position from 0
          do (let ((*in-slot?* t))
               (render (object-rendered-value object slot-name) stream))
          unless (= position (1- length))
            do (terpri stream))))

(defun render-base64-vector (vector stream)
  (encode-base64-bytes vector stream nil))

(defun render-object (object &key (stream nil) (emit-computed-values t) (emit-signatures t))
  (let ((*rewrite-references?* nil))
    (if (null stream)
        (with-output-to-string (stream)
          (render-object-to-stream object stream emit-computed-values emit-signatures))
        (render-object-to-stream object stream emit-computed-values emit-signatures))))

(defun write-signatures (signature-alist stream)
  (dolist (signature-pair signature-alist)
    (destructuring-bind (user . signature)
	signature-pair
      (write-string
       (etypecase user
	 (user (bytes->base64 (hash-object user)))
	 (reference (reference-hash user)))
       stream)
      (write-char #\Space stream)
      (write-string (bytes->base64 signature) stream)
      (write-char #\Newline stream))))

(defun maybe-write-computed-values (object stream)
  (flet ((write-computed-value-list (list)
           (write-char #\( stream)
           (loop for (computed-value . more?) on list
                 do (render-to-stream computed-value stream)
                    (when more?
                      (write-char #\Space stream)))
           (write-char #\) stream)))
    (let* ((class (class-of object))
           (slots (netfarm-class-computed-slot-names class)))
      (unless (zerop (length slots))
        (write-line "---" stream)
        (loop with length = (length slots)
              for slot-name across slots
              for position from 0
              do (write-computed-value-list
                  (object-computed-values object slot-name))
              unless (= position (1- length))
                do (terpri stream))))))

(defun maybe-write-vague-computed-values (vague-object stream)
  (let* ((table (vague-object-computed-values vague-object))
	 (names (sort (alexandria:hash-table-keys table) #'string<)))
    (unless (null names)
      (write-line "---" stream)
      (loop for (name . rest?) on names
            for values = (gethash name table)
            do (render-to-stream name stream)
               (write-char #\Space stream)
               (render-to-stream values stream)
            when rest? 
              do (write-char #\Newline stream)))))

(defgeneric render-object-to-stream (object stream emit-computed-values emit-signatures)
  (:documentation "Renders the Netfarm object OBJECT to a string.
Computed values will not be emitted if EMIT-COMPUTED-VALUES is false.
Signatures will not be emitted if EMIT-SIGNATURES is false.
Note that rendering a parsed block may not produce the exact same 
string; but RENDER-OBJECT will emit Netfarm standard format text."))

(defmethod render-object-to-stream ((object object) stream
                                    emit-computed-values
                                    emit-signatures)
  (when emit-signatures
    (write-signatures (object-signatures object)
		      stream))
  (write-line "---" stream)
  (write-string "\"schema\" " stream)
  (let ((*in-slot?* t))
    (render (netfarm-class-name (class-of object)) stream))
  (terpri stream)
  (write-line "---" stream)
  (render-slots object stream)
  (when emit-computed-values
    (maybe-write-computed-values object stream)))

(defmethod render-object-to-stream ((object vague-object) stream
                                    emit-computed-values
                                    emit-signatures)
  (when emit-signatures
    (write-signatures (vague-object-signatures object)
		      stream))
  (write-line "---" stream)
  (render-hash-table (vague-object-metadata object) stream)
  (unless (zerop (hash-table-count (vague-object-metadata object)))
    (terpri stream))
  (write-line "---" stream)
  (let ((*in-slot?* t))
    (render-hash-table (vague-object-values object) stream))
  (unless (zerop (hash-table-count (vague-object-values object)))
    (terpri stream))
  (when emit-computed-values
    (maybe-write-vague-computed-values object stream)))

(defun render (object &optional stream)
  "Render a Netfarm value to a string, or a stream if one is provided."
  (if (null stream)
      (with-output-to-string (stream)
        (render-to-stream object stream))
      (render-to-stream object stream)))

(defgeneric render-to-stream (object stream)
  (:documentation "Render a Netfarm value to a stream.")
  (:method ((ref reference) stream)
    (assert (stringp (reference-hash ref)))
    (format stream "ref:~a" (reference-hash ref)))
  (:method ((object object) stream)
    (format stream "ref:~a" (hash-object* object)))
  (:method ((string integer) stream)
    (format stream "integer:~d" string))
  (:method ((true  (eql ':true)) stream)
    (write-string "boolean:true" stream))
  (:method ((false (eql ':false)) stream)
    (write-string "boolean:false" stream))
  (:method ((unbound (eql ':unbound)) stream)
    (unless *in-slot?*
      (call-next-method))
    (write-string "~" stream))
  (:method ((string string) stream)
    (render-string string stream))
  (:method ((list list) stream)
    (write-char #\( stream)
    (loop with *in-slot?* = nil
          for (item . rest?) on list
          do (render-to-stream item stream)
             (unless (null rest?)
               (write-char #\Space stream)))
    (write-char #\) stream))
  (:method ((vector vector) stream)
    (write-string "base64-data:" stream)
    (s-base64:encode-base64-bytes vector stream nil)))
