(ql:quickload '(:drakma :jsown))

(defvar *bad-countries*
  '((("Australia") "Your node may be subject to the `Telecommunications Assistance and Access Bill 2018` bill; Please don't elect dumbasses who say `the laws of mathematics are very commendable, but the only law that applies in Australia is the law of Australia`.")
    (("Australia" "Canada" "United States" "United Kingdom" "New Zealand")
     "Your country is part of the Five Eyes surveillance alliance. These countries can and do share metadata about their citizens to circumvent privacy laws, and your node may be monitored.")
    (("United Kingdom") "Your node may be subject to the `Investigatory Powers Act 2016` act; you may be threatened with a prison sentence if you don't hand over keys you don't have.")))

(defvar *english-list*
  "~{~#[~;~a~;~a and ~a~:;~@{~a~#[~;, and ~:;, ~]~}~]~}")

(pushnew '("application" . "json") drakma:*text-content-types* :test #'equal)

(defun test-node-for-legalese ()
  (let* ((response (jsown:parse (drakma:http-request "http://ip-api.com/json/")))
         (country (jsown:val response "country"))
         (safe-p t))
    (format t "I think you are in ~a.~%===~%" country)
    (loop for (problematic-countries problem) in *bad-countries*
          when (member country problematic-countries :test #'string=)
            do (format t "Issue for ~a: ~a~%"
                       (format nil *english-list* problematic-countries)
                       problem)
               (setf safe-p nil))
    (write-line "===")
    (if (not safe-p)
        (write-line "You and your node may run into legal and/or surveillance issues.
There are a few things you could do, in order of preference:

- do it anyway; Netfarm is probably not a threat to anyone,
- clear up any issues with a lawyer or legal representative,
- set up your node as a Tor hidden service (https://www.torproject.org/docs/tor-onion-service),
- give it a go; you may be protected as you are only hosting law-violating content and did not create or curate it,
- refrain from running a node (sorry!)")
        (write-line "Congratulations! You should be able to run a Netfarm node without hinderance."))
    safe-p))

(write-line "You have loaded the Netfarm location tester; evaluate `(test-node-for-legalese)` to check the legal status of running a Netfarm node where you are currently. Please note:

- this program will connect to an external server (ip-api.com) to fetch your IP and geolocate it, and
- this is not an adequate substitute for legal advice; it may not have enough information about laws in your jurisdiction to make a correct statement.")
