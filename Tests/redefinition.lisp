(in-package :netfarm-tests)

(define-test redefinition :parent netfarm-tests)

(define-test redefine-class
  :parent redefinition
  (defclass a ()
    ()
    (:metaclass netfarm-class)
    (:scripts 1))                       ; totally a script
  (is equal '(1) (netfarm-class-scripts (find-class 'a)))
  (defclass a ()
    ()
    (:metaclass netfarm-class)
    (:scripts 2))
  (is equal '(2) (netfarm-class-scripts (find-class 'a))))
